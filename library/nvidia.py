#!/usr/bin/python

import subprocess
import xml.etree.ElementTree as element_tree
from ansible.module_utils.basic import AnsibleModule


ANSIBLE_METADATA = {'metadata_version': '1.0',
                    'status': ['stableinterface'],
                    'supported_by': 'kinetica'}


DOCUMENTATION = '''
---
module: nvidia
short_description: Gathers information about the GPU hardware and can modify some settings
description:
   - Queries hardware for GPU information
version_added: "0.1"
options:
  enabled:
    description:
      - Gathers facts about NVIDIA GPUs
    required: true
    default: null
    choices: [ "true", "false" ]

authors: 
    - "Michael Cramer <cramer@kinetica.com>"
    - "Scott Phelps <sphelps@kinetica.com>"
'''

EXAMPLES = '''
# Query GPU info and set persistence mode to enabled
- nvidia:
    persistence_mode: enabled
'''


def extract_field(node, facts, fields, prefix=None):
    '''
        Parameters:
            * node: a parsed XML ElementTree
            * facts: a dict of facts to populate and return
            * fields: a list of tuples containing the element and _type
        Description:
            Finds all elements in the provided tuples and formats,
            coercing them to the provided type,
            and formating with or without a prefix and underscore.
    '''
    for (element, _type) in fields:
        key = "{}_{}".format(prefix, element) if prefix else element
        facts[key] = _type(node.findall("./{}".format(element))[0].text)


def main():
    '''
        Main entrypoint for the module
    '''
    module = AnsibleModule(
        argument_spec=dict(
            enabled=dict(required=True, type='bool'))
    )
    facts = {}
    try:
        nvidia_smi = subprocess.Popen('nvidia-smi --query --xml-format',
                                      shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
        output, error = nvidia_smi.communicate()
        if nvidia_smi.returncode == 0:
            facts["nvidia_smi_installed"] = True
            root = element_tree.fromstring(output)
            extract_field(
                root, facts, [('driver_version', str), ('attached_gpus', int)], "nvidia")
            gpu_num = 0
            gpus = {}
            while True:
                gpu = root.findall("./gpu[minor_number='{}']".format(gpu_num))
                if gpu:
                    gpu_facts = {}
                    extract_field(gpu[0], gpu_facts, [
                        ('product_name', str),
                        ('product_brand', str),
                        ('display_mode', bool),
                        ('display_active', bool),
                        ('persistence_mode', bool),
                        ('accounting_mode', bool),
                        ('accounting_mode_buffer_size', int),
                        ('minor_number', int)
                    ])
                    gpus[gpu_num] = gpu_facts
                else:
                    break
                gpu_num += 1
            if bool(gpus):
                facts["nvidia_gpus"] = gpus
            module.exit_json(ansible_facts=facts)
        elif nvidia_smi.returncode == 127:
            # Don't fail if nvidia-smi is not installed, just exit gracefully...
            facts["nvidia_smi_installed"] = False
            module.exit_json(msg='nvidia-smi is not installed',
                             exit_code=nvidia_smi.returncode,
                             ansible_facts=facts)
        else: module.fail_json(msg="Unrecognized error code from nvidia-smi.",
                               meta={"error_code": nvidia_smi.returncode, "msg": error})

    except ValueError as error:
        module.fail_json(msg="Error executing nvidia-smi.", meta=error)


if __name__ == '__main__':
    main()